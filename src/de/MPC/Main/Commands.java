package de.MPC.Main;

import java.io.File;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.Song;
import com.xxmicloxx.NoteBlockAPI.SongPlayer;

import de.MPC.Minigames.MiniGame;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length >= 2 && args[0].equalsIgnoreCase("start")) {
			if(Main.m != null || !Main.canStart) {
				sender.sendMessage("�cEs ist bereits ein Minigame gestartet!");
				return true;
			}
			String name = args[1];
			for(int i = 2; i < args.length; i++) {
				name += " " + args[i];
			}
			MiniGame sel = null;
			for(MiniGame mg : Main.mgs) {
				if(mg.getName().equalsIgnoreCase(name)) sel = mg;
			}
			if(sel == null) {
				sender.sendMessage("�cMinigame existiert nicht!");
				return true;
			}
			
			Main.canStart = false;
			sender.sendMessage("�aMinigame wird gestartet!");
			
			final MiniGame g = sel;
			
			new BukkitRunnable() {
				public void run() {
					Bukkit.broadcastMessage(Main.PREFIX + "Es wird gespielt...");
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.playSound(p.getLocation(), Sound.BAT_TAKEOFF, 1, 1);
					}
				}
			}.runTaskLater(Main.plugin, 1*20);
			
			new BukkitRunnable() {
				public void run() {
					Bukkit.broadcastMessage("�8�m                                              ");
					Bukkit.broadcastMessage("�6   " + g.getName());
					
					for(String s : g.getDescription()) {
						Bukkit.broadcastMessage("�7    " + s);
					}
					
					Bukkit.broadcastMessage("�8�m                                              ");
					
					Bukkit.broadcastMessage(" ");
					
					Bukkit.broadcastMessage("�8Das Spiel beginnt in �73 Sekunden�8...");
					
					Song s = NBSDecoder.parse(new File(Main.plugin.getDataFolder(), "MiniGame Begin.nbs"));
					
					SongPlayer sp = new RadioSongPlayer(s);
					
					sp.setAutoDestroy(true);

					for(Player p : Bukkit.getOnlinePlayers()) {
						sp.addPlayer(p);
					}
					
					sp.setPlaying(true);
					
					new BukkitRunnable() {
						public void run() {
							start(g);
							
							cancel();
						}

					}.runTaskLater(Main.plugin, 3*20);
					
					cancel();
				}
			}.runTaskLater(Main.plugin, 3*20);
		} else if(args.length >= 1 && args[0].equalsIgnoreCase("stop")) {
			if(Main.m == null) {
				sender.sendMessage("�cKein Minigame gestartet!");
				return true;
			}
			sender.sendMessage("�aDas Minigame wird gestoppt!");
			Bukkit.broadcastMessage(Main.m.PREFIX + "�7Das Minigame stopt vorzeitig!");
			Main.m.end("null", "null", "null");
		}		
		
		return true;
	}
	
	public static void start(final MiniGame g) {
		Main.m  = g;
		
		g.doRandom();
		g.getLocation().getWorld().setTime(1000);
		
		new BukkitRunnable() {
			Iterator<? extends Player> op = Bukkit.getOnlinePlayers().iterator();
			@Override
			public void run() {
				if(!op.hasNext()) {
					for(final Player p : Bukkit.getOnlinePlayers()) {
						p.setLevel(0);
												
						for(PotionEffect pt : p.getActivePotionEffects()) {
							p.removePotionEffect(pt.getType());
						}
						
						p.sendMessage(Main.PREFIX + "Das Minigame beginnt!");
					}
										
					g.startself();
					cancel();
					return;
				}
				
				Player p = op.next();
				p.setFoodLevel(20);
				p.setMaxHealth(20D);
				p.setHealth(20.0);
				p.setAllowFlight(false);
				for(PotionEffect pt : p.getActivePotionEffects()) {
					p.removePotionEffect(pt.getType());
				}
				p.setLevel(0);
				p.setExp(0f);
				p.getInventory().clear();
				p.teleport(g.getLocation());
								
				p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
			}
		}.runTaskTimer(Main.plugin, 0, 5);
	}
	
	public static void end(final String p1, final String p2, final String p3) {		
		Main.m  = null;
				
		for(final Player p : Bukkit.getOnlinePlayers()) {
			p.setLevel(0);
			p.setFoodLevel(20);
			p.setHealth(20D);
			
			p.getInventory().setArmorContents(null);

			for(PotionEffect pt : p.getActivePotionEffects()) {
				p.removePotionEffect(pt.getType());
			}
		}
			
		new BukkitRunnable() {
			
			Iterator<? extends Player> op = Bukkit.getOnlinePlayers().iterator();
			
			@Override
			public void run() {
				if(!op.hasNext()) {
					
					new BukkitRunnable() {
						public void run() {							
							for(Player p : Bukkit.getOnlinePlayers()) {
								p.playSound(p.getLocation(), Sound.LEVEL_UP, 1, 1);
							}
							
							cancel();
						}
					}.runTaskLater(Main.plugin, 7*20);
					
					Main.canStart = true;
					
					cancel();
					return;
				}
				
				Player p = op.next();
				p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
			}
			
		}.runTaskTimer(Main.plugin, 0, 5);
	}

}
