package de.MPC.Main;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.potion.PotionEffect;

public class Listener implements org.bukkit.event.Listener {
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onLogin(PlayerLoginEvent e) {
		if(Main.m != null) {
			e.setKickMessage("�cEs wird im Momemt ein Minispiel gespielt!");
			e.setResult(Result.KICK_OTHER);
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		e.setJoinMessage("�8� �6" + p.getName() + "�7 tritt �6Mario Party �7bei �8[�7" + (Bukkit.getOnlinePlayers().size()) + "�8/�724�8]");
				
		if(p.isInsideVehicle()) {
			p.getVehicle().eject();
			p.getVehicle().remove();
		}
		
		p.setFoodLevel(20);
		p.setMaxHealth(20D);
		p.setHealth(20.0);
		p.setAllowFlight(false);

		for(PotionEffect pt : p.getActivePotionEffects()) {
			p.removePotionEffect(pt.getType());
		}
		
		p.setGameMode(GameMode.ADVENTURE);
		p.setLevel(0);
		p.setExp(0f);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
		p.setWalkSpeed(0.2f);
		e.setJoinMessage("");
		for(Player pt : Bukkit.getOnlinePlayers()) {
			if(p != pt) {
				p.showPlayer(pt);
				pt.showPlayer(p);
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		e.setQuitMessage(Main.PREFIX + "�6" + e.getPlayer().getName() + " �7verl�sst �6Mario Party!");
		if(Bukkit.getOnlinePlayers().size() -1 <= 0 && Main.m != null) {
			Bukkit.broadcastMessage(Main.m.PREFIX + "�7Das Minigame stopt vorzeitig!");
			Main.m.end("null", "null", "null");
		}
	}
}
