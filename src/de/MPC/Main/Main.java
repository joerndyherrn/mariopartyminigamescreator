package de.MPC.Main;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.xxmicloxx.NoteBlockAPI.NoteBlockPlayerMain;

import de.MPC.Minigames.GameEXAMPLE;
import de.MPC.Minigames.MiniGame;

public class Main extends JavaPlugin {

	public static Main plugin;
	public static ArrayList<MiniGame> mgs = new ArrayList<>();
	public static final String PREFIX = "�8[�6Party�8] �7";
	public static MiniGame m;
	public static boolean canStart = true;
	
	public void onEnable() {
		plugin = this;
		
		Bukkit.createWorld(new WorldCreator("games"));
		for(World w : Bukkit.getWorlds()) {
			w.setAutoSave(false);
			w.setThunderDuration(0);
			w.setThundering(false);
			w.setAnimalSpawnLimit(0);
			w.setAmbientSpawnLimit(0);
			w.setMonsterSpawnLimit(0);
			w.setWaterAnimalSpawnLimit(0);
			w.setTime(1000);
		}
		
		Bukkit.getPluginManager().registerEvents(new Listener(), this);
		Bukkit.getPluginCommand("mpc").setExecutor(new Commands());
		
		registerMinigames();
		
		NoteBlockPlayerMain.plugin = new NoteBlockPlayerMain();
	}
	
	public void onDisable() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer("lobby");
		}
		for(World w : Bukkit.getWorlds()) {
			Bukkit.unloadWorld(w, false);
		}
	}
	
	public static void registerMinigames() {
		mgs.add(new GameEXAMPLE());
	}
}
