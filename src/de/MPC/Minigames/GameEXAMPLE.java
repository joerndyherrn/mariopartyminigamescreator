package de.MPC.Minigames;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import de.MPC.utilities.BukkitSyncAutoTask;

public class GameEXAMPLE extends MiniGame {

	
	private int countdown = 61;
	
	/**
	 * TODO: Minigame in der Main.class in der registerMinigames() Methode registrieren
	 */
	public GameEXAMPLE() {
		super();
	}

	
	/**
	 * Startet das Minigame. Hier werden die Variablen unbedingt neu definiert!
	 */
	public void start() {
		countdown = 61;
		
		makeCountdown(10, new Runnable() {
			public void run() {
				new BukkitSyncAutoTask(20, 20) {
					@Override
					public void run() {
						if(canBroadcast(countdown)) {
							Bukkit.broadcastMessage(PREFIX + "�7Das Spiel endet in " + countdown + " Sekunden!");
						}
						if(countdown == 0) {
							stop();
							cancel();
						}
						countdown--;
					}
				};
			}
		}, PREFIX + "�7Noch %sec% Sekunden bis zum Start!", PREFIX + "�7Das Spiel startet jetzt!", true);
	}
	
	/**
	 * Stopt das Minigame. Hier werden die Gewinner ausgelost.
	 */
	public void stop() {
		Bukkit.broadcastMessage(PREFIX + "�7Das Spiel ist vorbei! Gewonnen hat...");
		
		new BukkitRunnable() {
			public void run() {
				endSound();
				
				@SuppressWarnings("serial")
				HashMap<String, Integer> points = new HashMap<String, Integer>() {{
					put("Test1", 100);
					put("Test2", 200);
					put("Test3", 400);
				}};
				
				
				final String first = getHighest(points);
				int firstpoints = points.get(first);
				points.remove(first);
				
				final String second = getHighest(points);
				int secondpoints = points.get(second);
				points.remove(second);
				
				final String third = getHighest(points);
				int thirdpoints = points.get(third);
				points.remove(third);
				
				Bukkit.broadcastMessage("�8�m                                              ");
				
				Bukkit.broadcastMessage("�6" + first + " hat �agewonnen");
				
				Bukkit.broadcastMessage(" ");
				Bukkit.broadcastMessage("�a#1 �7�l> �6" + first + " - " + firstpoints + " Punkte");
				Bukkit.broadcastMessage("�a#2 �7�l> �6" + second + " - " + secondpoints + " Punkte");
				Bukkit.broadcastMessage("�a#3 �7�l> �6" + third + " - " + thirdpoints + " Punkte");
				
				Bukkit.broadcastMessage("�8�m                                              ");
				
				Bukkit.broadcastMessage(" ");
				
				new BukkitRunnable() {
					public void run() {
						end(first, second, third);
					}
				}.runTaskLater(plugin, 50L);
				
				cancel();	
			}
		}.runTaskLater(plugin, 30L);
	}
	
	/**
	 * Definiert das Item, welches sp�ter in der Minigame �bersicht angezeigt wird.
	 */
	public ItemStack getItem() {
		return new ItemStack(Material.GRAVEL);
	}

	/**
	 * Definiert den Gamemode, welchen die Spieler w�hrend des Minigames haben sollen.
	 */
	public GameMode getGamemode() {
		return GameMode.ADVENTURE;
	}

	/**
	 * Definiert die Beschreibung des Minigames.
	 */
	public List<String> getDescription() {
		return null;
	}

	/**
	 * Definiert den Namen des Minigames.
	 */
	public String getName() {
		return "Beispiel Minigame";
	}

	/**
	 * Definiert die Location an welcher die Spieler spawnen.
	 */
	public Location getLocation() {
		return new Location(Bukkit.getWorld("games"), 0, 0, 0);
	}

	/**
	 * Diese Methode muss nicht �berschrieben werden. Falls es mehr als 1 Startlocation geben soll, kann man sie hier 'auslosen'.
	 */
	public void doRandom() {}
}
