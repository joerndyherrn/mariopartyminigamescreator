package de.MPC.Minigames;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.Song;
import com.xxmicloxx.NoteBlockAPI.SongPlayer;

import de.MPC.Main.Commands;
import de.MPC.Main.Main;
import de.MPC.utilities.BukkitSyncAutoTask;
import de.MPC.utilities.Reflection;

public abstract class MiniGame implements Listener {

	protected Main plugin;
	
	public final String PREFIX = "�8[�6" + getName() + "�8] �7";
	
	public MiniGame() {
		this.plugin = Main.plugin;
	}
	
	public void startself() {
		for(Entity e : getLocation().getWorld().getEntities()) {
			if(!(e instanceof Player)) {
				e.remove();
			}
		}
		for(Player p : getPlayers()) {
			p.setGameMode(getGamemode());
			p.setWalkSpeed(0.2f);
		}
		Bukkit.getPluginManager().registerEvents(this, plugin);
		start();
	}
	
	public void makeCountdown(final int seconds, final Runnable finished, final String message, final String stopmessage, final boolean exp) {
		new BukkitRunnable() {
			int maxsec = seconds;
			int s = seconds+1;
			boolean expdo = exp;
			String m = message;
			@Override
			public void run() {
				s--;
				if(s > 0) {
					if(expdo) {
						for(Player p : Bukkit.getOnlinePlayers()) {
							setEXPCount(s, maxsec, p);
						}
					}
					
					if(s % 15 == 0 || s == 10 || (s <= 5 && s > 0)) {
						Bukkit.broadcastMessage(m.replace("%sec%", s+""));
					}
				}else {
					if(expdo) {
						for(Player p : Bukkit.getOnlinePlayers()) {
							p.setLevel(0);
							p.setExp(0);
						}
					}
					
					Bukkit.broadcastMessage(stopmessage);
					finished.run();
					cancel();
				}
			}
		}.runTaskTimer(Main.plugin, 0, 20);
	}
	
	public void doRandom() {}
	
	public abstract void start();
	
	public abstract void stop();
	
	public abstract ItemStack getItem();
	
	public abstract GameMode getGamemode();
	
	public ItemStack getItemWithMaterialAndData(Material m, byte data) {
		ItemStack is = new ItemStack(m, 1, data);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		lore.addAll(Arrays.asList("�6Minispiel: �7" + getName(), "�8�m                                              "));
		boolean first = true;
		for(String s : getDescription()) {
			if(first) {
				lore.add("�6Beschreibung: �7" + s);
				first = false;
			}else {
				lore.add("  �7" + s);
			}
		}
		im.setLore(lore);
		im.setDisplayName("�6" + getName());
		is.setItemMeta(im);
		return is;
	}
	
	public ItemStack getItemWithMaterial(Material m) {
		ItemStack is = new ItemStack(m);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		lore.addAll(Arrays.asList("�6Minispiel: �7" + getName(), "�8�m                                              "));
		boolean first = true;
		for(String s : getDescription()) {
			if(first) {
				lore.add("�6Beschreibung: �7" + s);
				first = false;
			}else {
				lore.add("  �7" + s);
			}
		}
		im.setLore(lore);
		im.setDisplayName("�6" + getName());
		is.setItemMeta(im);
		return is;
	}
	
	public boolean canBroadcast(int time) {
		if(time % 15 == 0 || time == 10 || (time <= 5 && time > 0)) return true;
		return false;
	}
	
	public void end(String p1, String p2, String p3) {
		
		try {
			HandlerList.unregisterAll(this);
		} catch(Exception ex) {}

		for(Player p : getPlayers()) {
			Object nmsPlayer = Reflection.getNMSPlayer(p);
			p.setGameMode(GameMode.ADVENTURE);
			p.setMaxHealth(20D);
			p.setHealth(20D);
			p.setLevel(0);
			p.setExp(0);
			p.setWalkSpeed(0.2f);
			p.setFlying(false);
			p.setAllowFlight(false);
			p.getInventory().clear();
			p.updateInventory();
			try {
				Object dw = nmsPlayer.getClass().getMethod("getDataWatcher").invoke(nmsPlayer);
				dw.getClass().getMethod("watch", int.class, Object.class).invoke(dw, 9, (byte)0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for(Entity e : getLocation().getWorld().getEntities()) {
			if(!(e instanceof Player)) {
				e.remove();
			}
		}
		Commands.end(p1, p2, p3);
	}
	
	public void endSound() {
		Song s = NBSDecoder.parse(new File(plugin.getDataFolder(), "MiniGame End.nbs"));
		
		SongPlayer sp = new RadioSongPlayer(s);
		
		sp.setAutoDestroy(true);

		for(Player p : Bukkit.getOnlinePlayers()) {
			p.setLevel(0);
			p.setExp(0);
			sp.addPlayer(p);
		}
		
		sp.setPlaying(true);
	}
	
	public List<Player> getPlayers() {
		List<Player> pp = new ArrayList<Player>();
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			pp.add(p);
		}
		Collections.shuffle(pp);
		return pp;
	}
	
	public abstract List<String> getDescription();

	public abstract String getName();
	
	public abstract Location getLocation();
	
	public static void setEXPCountdown(final Player p, final int secs) {
		final int full = secs;
		new BukkitSyncAutoTask(0, secs*20) {
			int seconds = secs;
			@Override
			public void run() {
				if(seconds == 0) {
					cancel();
					return;
				}
				setEXPCount(seconds, full, p);
				seconds--;
			}
		};
	}
	
	public static void setEXPCount(double seconds, double fullseconds, Player p) {		
		p.setLevel((int) seconds);
		double exp = seconds/fullseconds;
		p.setExp((float) exp);
	}
	
	public static boolean percent(int prozent) {
		if(new Random().nextInt(100) <= prozent) return true;
		return false;
	}
	
	public static String getLowest(HashMap<String, ? extends Number> hashmap) {
		if(hashmap == null || hashmap.isEmpty()) return "-";
		Number t = null;
		String s = "-";
		for(Number l : hashmap.values()) {
			if(t == null) {
				t = l;
				s = (String) getKey(hashmap, l);
			}
			if(l instanceof Integer) {
				if(l.intValue() < t.intValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Long) {
				if(l.longValue() < t.longValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Float) {
				if(l.floatValue() < t.floatValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
				
			}else if(l instanceof Byte) {
				if(l.byteValue() < t.byteValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Double) {
				if(l.doubleValue() < t.doubleValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Short) {
				if(l.shortValue() < t.shortValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}
			
		}
		return s;
	}
	
	public static String getHighest(HashMap<String, ? extends Number> hashmap) {
		if(hashmap == null || hashmap.isEmpty()) return "-";
		Number t = null;
		String s = "-";
		for(Number l : hashmap.values()) {
			if(t == null) {
				t = l;
				s = (String) getKey(hashmap, l);
			}
			if(l instanceof Integer) {
				if(l.intValue() > t.intValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Long) {
				if(l.longValue() > t.longValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Float) {
				if(l.floatValue() > t.floatValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Byte) {
				if(l.byteValue() > t.byteValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Double) {
				if(l.doubleValue() > t.doubleValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}else if(l instanceof Short) {
				if(l.shortValue() > t.shortValue()) {
					t = l;
					s = (String) getKey(hashmap, l);
				}
			}
			
		}
		return s;
	}
	
	public static Object getKey(HashMap<?, ?> hashmap, Object value) {
		for(Object o : hashmap.keySet()) {
			if(hashmap.get(o).equals(value)) return o;
		}
		return null;
	}
	
	public static int rdmInt(int minInt, int maxInt) {
		Random r = new Random();
		return r.nextInt((maxInt - minInt) + 1) + minInt;
	}
}
