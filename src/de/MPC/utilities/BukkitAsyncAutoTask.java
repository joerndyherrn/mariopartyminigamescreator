package de.MPC.utilities;

import org.bukkit.scheduler.BukkitRunnable;

import de.MPC.Main.Main;

public abstract class BukkitAsyncAutoTask extends BukkitRunnable {
	
	public BukkitAsyncAutoTask() {
		if(Main.plugin.isEnabled()) {
			this.runTaskAsynchronously(Main.plugin);
		}else {
			this.run();
		}
	}
	
	public BukkitAsyncAutoTask(long delay) {
		this.runTaskLaterAsynchronously(Main.plugin, delay);
	}
	
	public BukkitAsyncAutoTask(long delay, long period) {
		this.runTaskTimerAsynchronously(Main.plugin, delay, period);
	}
}
