package de.MPC.utilities;

import org.bukkit.scheduler.BukkitRunnable;

import de.MPC.Main.Main;

public abstract class BukkitSyncAutoTask extends BukkitRunnable{
	
	public BukkitSyncAutoTask() {
		if(Main.plugin.isEnabled()) {
			this.runTask(Main.plugin);
		}else {
			this.run();
		}
	}
	
	public BukkitSyncAutoTask(long delay) {
		this.runTaskLater(Main.plugin, delay);
	}
	
	public BukkitSyncAutoTask(long delay, long period) {
		this.runTaskTimer(Main.plugin, delay, period);
	}
}
