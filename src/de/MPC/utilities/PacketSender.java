package de.MPC.utilities;

import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PacketSender {

	public static Object getGameProfile(String uuid, String name) {
		try {
			Object gameprofile = null;
			try {
				gameprofile = Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile").getConstructor(String.class, String.class).newInstance(uuid, name);
			}catch (Exception e) {
				gameprofile = Class.forName("net.minecraft.util.com.mojang.authlib.GameProfile").getConstructor(UUID.class, String.class).newInstance(getUUIDFromString(uuid), name);
			}
			return gameprofile;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static UUID getUUIDFromString(String uuid) {
		if(uuid.length() < 32) return null;
		if(!uuid.contains("-")) uuid = uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + uuid.substring(12, 16) + "-" + uuid.substring(16, 20) + "-" + uuid.substring(20, 32);
		return UUID.fromString(uuid);
	}
	
	public static void sendPacket(List<Player> players, String packetName, Class<?>[] parameterclass, Object... parameters) {
		for(Player p : players) {
			sendPacket(p, packetName, parameterclass, parameters);
		}
	}
	
	public static void sendPacket(Player p, String packetName, Class<?>[] parameterclass, Object... parameters) {
		try {
			Object nmsPlayer = Reflection.getNMSPlayer(p);
			Object connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
			Object packet = Class.forName(nmsPlayer.getClass().getPackage().getName() + "." + packetName).getConstructor(parameterclass).newInstance(parameters);
			for(Method m : connection.getClass().getMethods()) {
				if(m.getParameterTypes().length == 1 && m.getName().equalsIgnoreCase("sendPacket")) {
					m.setAccessible(true);
					m.invoke(connection, packet);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendPacket(Player p, Object packet) {
		try {
			Object nmsPlayer = Reflection.getNMSPlayer(p);
			Object connection = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
			for(Method m : connection.getClass().getMethods()) {
				if(m.getParameterTypes().length == 1 && m.getName().equalsIgnoreCase("sendPacket")) {
					m.setAccessible(true);
					m.invoke(connection, packet);
					break;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendToAll(String packetName, Class<?>[] parameterclass, Object... parameters) {
		for(Player p : Bukkit.getOnlinePlayers())
			sendPacket(p, packetName, parameterclass, parameters);
	}
	
	public static void sendToAll(Object packet) {
		for(Player p : Bukkit.getOnlinePlayers())
			sendPacket(p, packet);
	}
}
