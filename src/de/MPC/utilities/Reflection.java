package de.MPC.utilities;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Reflection {
	
	public static String getRawVersion() {
		return Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}
	
	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}

	public static Object getNMSPlayer(Player p) {
		try {
			return p.getClass().getMethod("getHandle").invoke(p);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Class<?> getNMSClass(String className) {
		String fullName = "net.minecraft.server." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}

	public static Class<?> getOBCClass(String className) {
		String fullName = "org.bukkit.craftbukkit." + getVersion() + className;
		Class<?> clazz = null;
		try {
			clazz = Class.forName(fullName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clazz;
	}

	public static Object getHandle(Entity obj) {
		try {
			return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Field getField(Class<?> clazz, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
		for (Method m : clazz.getMethods()) {
			if ((m.getName().equals(name)) && ((args.length == 0) || (ClassListEqual(args, m.getParameterTypes())))) {
				m.setAccessible(true);
				return m;
			}
		}
		return null;
	}

	public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
		boolean equal = true;
		if (l1.length != l2.length) {
			return false;
		}
		for (int i = 0; i < l1.length; i++) {
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}
	
	public static void setValue(Object instance, String fieldName, Object value) throws Exception {
		Field field = instance.getClass().getDeclaredField(fieldName);
	    field.setAccessible(true);
	    field.set(instance, value);
	}

	public static Object getValue(Object instance, String fieldName) throws Exception {
	    Field field = instance.getClass().getDeclaredField(fieldName);
	    field.setAccessible(true);
	    return field.get(instance);
	}
	
	public static Object getClass(String name, Object[] args) throws Exception {
		Class<?> c = Class.forName(getPackageName() + "." + name);
		int params = 0;
		if(args != null) {
			params = args.length;
		}
		
		for(Constructor<?> co : c.getConstructors()) {
			if(co.getParameterTypes().length == params) {
				return co.newInstance(args);
			}
		}
		
		return null;
	}
	
	public static Method getMethod(String name, Class<?> cl, int params) {
		for(Method m : cl.getMethods()) {
			if((m.getName().equals(name)) && (m.getParameterTypes().length == params)) {
				m.setAccessible(true);
				return m;
			}
		}
		
		return null;
	}
	
	public static <T> T getFieldValue(Class<?> src, String name, Class<T> type, Object from) throws SecurityException, NoSuchFieldException {
		Field field = src.getDeclaredField(name);
		field.setAccessible(true);
		
		try{
			return type.cast(field.get(from));
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static <T> T invokeMethod(Class<?> src, String name, Class<T> returnType, Object in, Class<?>[] args, Object[] params) throws SecurityException, NoSuchMethodException {
		Method method = src.getDeclaredMethod(name, args);
		method.setAccessible(true);
		
		try{
			return returnType.cast(method.invoke(in, params));
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String getPackageName() {
		return "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}
}
