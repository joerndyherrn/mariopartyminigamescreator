package de.MPC.utilities;

import java.lang.reflect.Field;

import org.bukkit.entity.Player;

public class TitleManager {
	
	private static Class<?> title = Reflection.getNMSClass("PacketPlayOutTitle");
	private static Class<?> enumtitleaction = Reflection.getNMSClass("EnumTitleAction");
	private static Class<?> chatserial = Reflection.getNMSClass("ChatSerializer");
	
	public static void sendActionBar(Player p, String text) {
		try {
			PacketSender.sendPacket(p, "PacketPlayOutChat", new Class[] { Reflection.getNMSClass("IChatBaseComponent"), byte.class }, chatserial.getMethod("a", String.class).invoke(null, "{\"text\": \"" + text + "\"}"), (byte)2);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendHeaderAndFooter(Player p, String header, String footer) {
		try {
			Object packet = Reflection.getNMSClass("PacketPlayOutPlayerListHeaderFooter").newInstance();
			getField(packet.getClass().getDeclaredField("a")).set(packet, chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + header + "'}"));
			getField(packet.getClass().getDeclaredField("b")).set(packet, chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + footer + "'}"));
			PacketSender.sendPacket(p, packet);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendTitles(Player p, String title, String subtitle, int einblenden, int bleiben, int ausblenden) {
		sendTimings(p, einblenden, bleiben, ausblenden);
		sendTitles(p, title, subtitle);
	}
	
	public static void sendTitles(Player p, String title, String subtitle) {
		sendTitle(p, title);
		sendSubTitle(p, subtitle);
	}
	
	public static void sendTitle(Player p, String title, int einblenden, int bleiben, int ausblenden) {
		sendTimings(p, einblenden, bleiben, ausblenden);
		sendTitle(p, title);
	}
	
	public static void sendTitle(Player p, String title) {
		try {
			Object t = TitleManager.title.newInstance();
			Field f = t.getClass().getDeclaredField("a");
			f.setAccessible(true);
			f.set(t, getField(enumtitleaction.getDeclaredField("TITLE")).get(null));
			f = t.getClass().getDeclaredField("b");
			f.setAccessible(true);
			f.set(t, chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + title + "'}"));
			PacketSender.sendPacket(p, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendSubTitle(Player p, String subtitle, int einblenden, int bleiben, int ausblenden) {
		sendTimings(p, einblenden, bleiben, ausblenden);
		sendSubTitle(p, subtitle);
	}
	
	public static void sendSubTitle(Player p, String subtitle) {
		try {
			Object t = title.newInstance();
			Field f = t.getClass().getDeclaredField("a");
			f.setAccessible(true);
			f.set(t, getField(enumtitleaction.getDeclaredField("SUBTITLE")).get(null));
			f = t.getClass().getDeclaredField("b");
			f.setAccessible(true);
			f.set(t, chatserial.getMethod("a", String.class).invoke(null, "{'text': '" + subtitle + "'}"));
			PacketSender.sendPacket(p, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendTimings(Player p, int einblenden, int bleiben, int ausblenden) {
		try {
			Object t = title.newInstance();
			Field f = t.getClass().getDeclaredField("a");
			f.setAccessible(true);
			f.set(t, getField(enumtitleaction.getDeclaredField("TIMES")).get(null));
			f = t.getClass().getDeclaredField("c");
			f.setAccessible(true);
			f.set(t, einblenden);
			f = t.getClass().getDeclaredField("d");
			f.setAccessible(true);
			f.set(t, bleiben);
			f = t.getClass().getDeclaredField("e");
			f.setAccessible(true);
			f.set(t, ausblenden);
			PacketSender.sendPacket(p, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void reset(Player p) {
		try {
			Object t = title.newInstance();
			Field f = t.getClass().getDeclaredField("a");
			f.setAccessible(true);
			f.set(t, getField(enumtitleaction.getDeclaredField("RESET")).get(null));
			PacketSender.sendPacket(p, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void clear(Player p) {
		try {
			Object t = title.newInstance();
			Field f = t.getClass().getDeclaredField("a");
			f.setAccessible(true);
			f.set(t, getField(enumtitleaction.getDeclaredField("CLEAR")).get(null));
			PacketSender.sendPacket(p, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Field getField(Field f) {
		f.setAccessible(true);
		return f;
	}
}
